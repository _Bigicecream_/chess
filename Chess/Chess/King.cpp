#include "Chess.h"

King::King(bool player,int y,int x)
{
	this->_player = player;
	this->_y = y;
	this->_x = x;
}

bool King::move(int from[2], int to[2], Board* brdptr)
{
	Piece* ptr;
	//check if move is legal
	if (from[0] - to[0] > 1)
	{
		return false;
	}
	else if (to[0] - from[0] > 1)
	{
		return false;
	}
	else if (from[1] - to[1] > 1)
	{
		return false;
	}
	else if (to[1] - from[1] > 1)
	{
		return false;
	}

	//check if a friendly is in the way
	if (brdptr->at(to[0], to[1]) != nullptr && brdptr->at(to[0], to[1])->player() == this->_player)
	{
		return false;
	}

	ptr = brdptr->at(to[0], to[1]);
	brdptr->place(this, to[0], to[1]);
	brdptr->place(nullptr, from[0], from[1]);

	//check if there's a check(haha)
	if (brdptr->check(!this->_player) == true)
	{
		brdptr->place(ptr, to[0], to[1]);
		brdptr->place(this, from[0], from[1]);
		return false;
	}
	if (ptr != nullptr)
	{
		delete[] ptr;
	}
	return true;
}

bool King::check(int position[2],Board* brdptr)
{
	if (this->_player == white)
	{
		if (position[0] > 0)
		{
			if (brdptr->at(position[0] - 1, position[1]) != nullptr)
			{
				if (brdptr->at(position[0] - 1, position[1])->type() == 'm')
				{
					return true;
				}
			}
			if (position[1] < 7)
			{
				if (brdptr->at(position[0] - 1, position[1] + 1) != nullptr)
				{
					if (brdptr->at(position[0] - 1, position[1] + 1)->type() == 'm')
					{
						return true;
					}
				}
			}
			if (position[1] > 0)
			{
				if (brdptr->at(position[0] - 1, position[1] - 1) != nullptr)
				{
					if (brdptr->at(position[0] - 1, position[1] - 1)->type() == 'm')
					{
						return true;
					}
				}
			}
		}

		if (position[0] < 7)
		{
			if (brdptr->at(position[0] + 1, position[1]) != nullptr)
			{
				if (brdptr->at(position[0] + 1, position[1])->type() == 'm')
				{
					return true;
				}
			}			
			if (position[1] > 0)
			{
				if (brdptr->at(position[0] + 1, position[1] - 1) != nullptr)
				{
					if (brdptr->at(position[0] + 1, position[1] - 1)->type() == 'm')
					{
						return true;
					}
				}
			}
			if (position[1] < 7)
			{
				if (brdptr->at(position[0] + 1, position[1] + 1))
				{
					if (brdptr->at(position[0] + 1, position[1] + 1)->type() == 'm')
					{
						return true;
					}
				}
			}
		}
		if (position[1] < 7)
		{
			if (brdptr->at(position[0], position[1] + 1))
			{
				if (brdptr->at(position[0], position[1] + 1)->type() == 'm')
				{
					return true;
				}
			}
		}
		if (position[1] > 0)
		{
			if (brdptr->at(position[0], position[1] - 1))
			{
				if (brdptr->at(position[0], position[1] - 1)->type() == 'm')
				{
					return true;
				}
			}
		}
	}
	else
	{
		if (position[0] > 0)
		{
			if (brdptr->at(position[0] - 1, position[1]) != nullptr)
			{
				if (brdptr->at(position[0] - 1, position[1])->type() == 'M')
				{
					return true;
				}
			}
			if (position[1] < 7)
			{
				if (brdptr->at(position[0] - 1, position[1] + 1) != nullptr)
				{
					if (brdptr->at(position[0] - 1, position[1] + 1)->type() == 'M')
					{
						return true;
					}
				}
			}
			if (position[1] > 0)
			{
				if (brdptr->at(position[0] - 1, position[1] - 1) != nullptr)
				{
					if (brdptr->at(position[0] - 1, position[1] - 1)->type() == 'M')
					{
						return true;
					}
				}
			}
		}

		if (position[0] < 7)
		{
			if (brdptr->at(position[0] + 1, position[1]) != nullptr)
			{
				if (brdptr->at(position[0] + 1, position[1])->type() == 'M')
				{
					return true;
				}
			}
			if (position[1] > 0)
			{
				if (brdptr->at(position[0] + 1, position[1] - 1) != nullptr)
				{
					if (brdptr->at(position[0] + 1, position[1] - 1)->type() == 'M')
					{
						return true;
					}
				}
			}
			if (position[1] < 7)
			{
				if (brdptr->at(position[0] + 1, position[1] + 1))
				{
					if (brdptr->at(position[0] + 1, position[1] + 1)->type() == 'M')
					{
						return true;
					}
				}
			}
		}
		if (position[1] < 7)
		{
			if (brdptr->at(position[0], position[1] + 1))
			{
				if (brdptr->at(position[0], position[1] + 1)->type() == 'M')
				{
					return true;
				}
			}
		}
		if (position[1] > 0)
		{
			if (brdptr->at(position[0], position[1] - 1))
			{
				if (brdptr->at(position[0], position[1] - 1)->type() == 'M')
				{
					return true;
				}
			}
		}
	}
	return false;
}

char King::type()
{
	if (this->_player == white)
	{
		return 'M';
	}
	else
	{
		return 'm';
	}
}