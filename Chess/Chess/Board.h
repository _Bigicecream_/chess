#ifndef BOARD
#define BOARD

#include <windows.h>
#include <string>
#include "Piece.h"
using namespace std;

#define white true
#define black false

class Board
{
private:
	Piece* board[8][8];

public:
	Piece* at(int y, int x);
	Board();
	~Board();
	Board(string saveName,int* flag);//loding the save
	bool check();
	bool check(bool player);
	void print();
	int save(string saveName,bool player);
	bool Board::move(int from[2],int to[2], bool player);
	void place(Piece*,int y, int x);
	void game(bool player);
	bool checkmate(bool player);
};

#endif