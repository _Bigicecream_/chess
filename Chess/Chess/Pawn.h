#ifndef PAWN
#define PAWN

#include <string>
#include "Piece.h"
using namespace std;

class Pawn : public Piece
{
private:
	bool _moved;
public:
	bool moved();
	Pawn(bool player, int y, int x);
	bool check(int position[2],Board* brdptr);
	char type();
	bool move(int from[2], int to[2], Board* brdptr);
};

#endif