#include "Chess.h"
#include <stdlib.h>


int main()
{
	CreateDirectory("saves", NULL);
	CreateDirectory("moves", NULL);
	Board* brd;
	string str;
	int flg;
	int* flag = &flg;
	int query, query2;
	system("color 80");
	while (true)
	{
		system("CLS");
		cout << "Welcome to the best chess game ever!" << endl;
		cout << "Please choose an option: " << endl;
		cout << "1.New game" << endl;
		cout << "2.Load saved games" << endl;
		cout << "3.Load game from a moves file" << endl;
		cout << "4.Playing on the internet" << endl;
		cout << "5.Quit" << endl;
		cin >> query;
		switch (query)
		{
		case 1:
			brd = new Board();
			brd->game(white);
			brd->~Board();
			break;
		case 2:
			cout << "Which file would you like to load? (example: save1)" << endl;
			cin >> str;
			brd = new Board(str,flag);
			if (*flag == -1)
			{
				cout << "Unable to load save" << endl;
			}
			else
			{
				if (*flag == 1)
				{
					brd->game(white);
					brd->~Board();
				}
				else
				{
					brd->game(black);
					brd->~Board();
				}
			}
			
			break;
		case 3:
			brd = new Board();
			cout << "Which moves file would you like to load? (example: moves)" << endl;
			cin >> str;
			readMovesFromFile(str,brd);
			system("PAUSE");
			break;
		case 4:
			cout << "1.Host" << endl;
			cout << "2.Guest" << endl;
			cin >> query2;
			switch (query2)
			{
			case 1:
				/////////
				break;
			case 2:
				//////
				break;
			default:
				break;
			}
			break;
		case 5:
			exit(0);
		default:
			break;
		}
	}
	
	system("PAUSE");
}
