#include "Chess.h"

Pawn::Pawn(bool player,int y, int x)
{
	this->_player = player;
	this->_moved = false;
	this->_y = y;
	this->_x = x;
}

bool Pawn::check(int position[2], Board* brdptr)
{
	if (this->_player == black)
	{
		if (position[0] > 0)
		{
			if (position[1] < 7)
			{
				if (brdptr->at(position[0] + 1, position[1] + 1) != nullptr)
				{
					if (brdptr->at(position[0] + 1, position[1] + 1)->type() == 'M')
					{
						return true;
					}
				}
			}
			if (position[1] > 0)
			{
				if (brdptr->at(position[0] + 1, position[1] - 1) != nullptr)
				{
					if (brdptr->at(position[0] + 1, position[1] - 1)->type() == 'M')
					{
						return true;
					}
				}
			}
		}
	}
	else
	{
		if (position[0] < 7)
		{
			if (position[1] < 7)
			{
				if (brdptr->at(position[0] - 1, position[1] + 1) != nullptr)
				{
					if (brdptr->at(position[0] - 1, position[1] + 1)->type() == 'm')
					{
						return true;
					}
				}
			}
			if (position[1] > 0)
			{
				if (brdptr->at(position[0] - 1, position[1] - 1) != nullptr)
				{
					if (brdptr->at(position[0] - 1, position[1] - 1)->type() == 'm')
					{
						return true;
					}
				}
			}
		}
	}
	return false;
}

bool Pawn::move(int from[2], int to[2], Board* brdptr)
{
	Piece* ptr;
	if (from[0] == to[0] && from[1] == to[1])
	{
		return false;
	}
	if (this->_player==white)
	{
		if (from[0] < to[0])
		{
			return false;
		}
			if (from[1] == to[1])//if  dont moveing on x
			{
				if (from[0] - to[0] > 1 && this->_moved == true)
				{
					return false;
				}
				else if (from[0] - to[0] > 2)
				{
					return false;
				}
				if (brdptr->at(to[0], to[1]) != nullptr)//if it enmy
				{
					return false;
				}
				if (this->_moved == false && from[0] - to[0] == 2)//if it enmy
				{
					if (brdptr->at(to[0], to[1]) != nullptr||brdptr->at(to[0]+1, to[1]) != nullptr)//if it enmy
					{
						return false;
					}
				}

			}
			else if (to[1] == from[1] - 1 || to[1] == from[1] + 1)//if moveing on x only 1
			{
				if (from[0] - to[0] == 1)////if moveing on y only 1
				{
					if (brdptr->at(to[0], to[1]) != nullptr)//if it enmy
					{
						if (brdptr->at(to[0], to[1])->player() == this->_player)
						{
							return false;
						}

					}
				}
				else
				{
					return false;
				}
			}
		
	}
	if (this->_player == black)
	{
		if (from[1] == to[1])//if  dont moveing on x
		{
			if (from[0] > to[0])
			{
				return false;
			}
			if (from[0] - to[0] < -1 && this->_moved == true)
			{
				return false;
			}
			else if (from[0] - to[0] < -2)
			{
				return false;
			}
			if (brdptr->at(to[0], to[1]) != nullptr)//if it enmy
			{
				return false;
			}
			if (this->_moved == false && from[0] - to[0] == 2)//if it enmy
			{
				if (brdptr->at(to[0], to[1]) != nullptr||brdptr->at(to[0] - 1, to[1]) != nullptr)//if it enmy
				{
					return false;
				}
			}

		}
		else if (to[1] == from[1] - 1 || to[1] == from[1] + 1)//if moveing on x only 1
		{
			if (from[0] - to[0] == -1)////if moveing on y only 1
			{
				if (brdptr->at(to[0], to[1]) != nullptr )//if it enmy
				{
					if (brdptr->at(to[0], to[1])->player() == this->_player)
					{
						return false;
					}
				}
			}
			else
			{
				return false;
			}
		}

	}
	ptr = brdptr->at(to[0], to[1]);
	brdptr->place(brdptr->at(from[0], from[1]), to[0], to[1]);
	brdptr->place(nullptr, from[0], from[1]);
	if (this->_moved == false)
	{
		this->_moved = true;
	}
	if (brdptr->check(!this->_player) == true)
	{
		brdptr->place(this, from[0], from[1]);
		brdptr->place(ptr, to[0], to[1]);
		return false;
	}

	if (ptr != nullptr)
	{
		delete[] ptr;
	}
	return true;
	
}

char Pawn::type()
{
	if (this->_player == white)
	{
		return 'P';
	}
	else
	{
		return 'p';
	}
}

bool Pawn::moved()
{
	return this->_moved;
}