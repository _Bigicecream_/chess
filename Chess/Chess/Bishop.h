#ifndef BISHOP_H
#define BISHOP_H

#include <string>
#include "Piece.h"

class Bishop : public Piece
{
public:
	Bishop(bool player,int y, int x);
	bool move(int from[2], int to[2], Board* brdptr);
	bool check(int position[2], Board* brdptr);
	char type();
};

#endif // !BISHOP_H