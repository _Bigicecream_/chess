#ifndef ROOK
#define ROOK

#include <string>
#include "Piece.h"
using namespace std;

class Rook : public Piece
{
public:
	Rook(bool player,int y,int x);
	bool check(int position[2],Board* brdptr);
	char type();
	bool move(int from[2], int to[2], Board* brdptr);
};

#endif