#include "Chess.h"

Queen::Queen(bool player,int y, int x)
{
	this->_player = player;
	this->_y = y;
	this->_x = x;
}

bool Queen::check(int position[2], Board* brdptr)
{
	int i, j;
	if (this->_player == white)
	{
		/*BISHOP*//////////////////////////////////////////////////////////
		//rightup
		i = position[0] - 1;
		j = position[1] + 1;
		while (i >= 0 && j <= 7)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'm')
				{
					return true;
				}
				break;
			}
			i--;
			j++;
		}

		//leftdown
		i = position[0] + 1;
		j = position[1] - 1;
		while (i <= 7 && j >= 0)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'm')
				{
					return true;
				}
				break;
			}
			i++;
			j--;
		}

		//rightdown
		i = position[0] + 1;
		j = position[1] + 1;
		while (i <= 7 && j <= 7)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'm')
				{
					return true;
				}
				break;
			}
			i++;
			j++;
		}

		//leftup
		i = position[0] - 1;
		j = position[1] - 1;
		while (i >= 0 && j >= 0)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'm')
				{
					return true;
				}
				break;
			}
			i--;
			j--;
		}
		/*END BISHOP*/////////////////////////////////////////////////////

		/*ROOK*//////////////////////////////////////////////////////////
		//up

		for (i = position[0] - 1; i >= 0; i--)
		{
			if (brdptr->at(i, position[1]) != nullptr)
			{
				if (brdptr->at(i, position[1])->type() == 'm')
				{
					return true;
				}
				break;
			}
		}

		//down
		for (i = position[0] + 1; i <= 7; i++)
		{
			if (brdptr->at(i, position[1]) != nullptr)
			{
				if (brdptr->at(i, position[1])->type() == 'm')
				{
					return true;
				}
				break;
			}
		}

		//left
		for (i = position[1] - 1; i >= 0; i--)
		{
			if (brdptr->at(position[0], i) != nullptr)
			{
				if (brdptr->at(position[0], i)->type() == 'm')
				{
					return true;
				}
				break;
			}
		}

		//right
		for (i = position[1] + 1; i <= 7; i++)
		{
			if (brdptr->at(position[0], i) != nullptr)
			{
				if (brdptr->at(position[0], i)->type() == 'm')
				{
					return true;
				}
				break;
			}
		}
		/*END ROOK*//////////////////////////////
	}

	else
	{
		//rightup
		i = position[0] - 1;
		j = position[1] + 1;
		while (i >= 0 && j <= 7)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'M')
				{
					return true;
				}
				break;
			}
			i--;
			j++;
		}
		//leftdown
		i = position[0] + 1;
		j = position[1] - 1;
		while (i <= 7 && j >= 0)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'M')
				{
					return true;
				}
				break;
			}
			i++;
			j--;
		}
		//rightdown
		i = position[0] + 1;
		j = position[1] + 1;
		while (i <= 7 && j <= 7)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'M')
				{
					return true;
				}
				break;
			}
			i++;
			j++;
		}
		//leftup
		i = position[0] - 1;
		j = position[1] - 1;
		while (i >= 0 && j >= 0)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'M')
				{
					return true;
				}
				break;
			}
			i--;
			j--;
		}

		//up
		for (i = position[0] - 1; i >= 0; i--)
		{
			if (brdptr->at(i, position[1]) != nullptr)
			{
				if (brdptr->at(i, position[1])->type() == 'M')
				{
					return true;
				}
				break;
			}
		}

		//down
		for (i = position[0] + 1; i <= 7; i++)
		{
			if (brdptr->at(i, position[1]) != nullptr)
			{
				if (brdptr->at(i, position[1])->type() == 'M')
				{
					return true;
				}
				break;
			}
		}

		//left
		for (i = position[1] - 1; i >= 0; i--)
		{
			if (brdptr->at(position[0], i) != nullptr)
			{
				if (brdptr->at(position[0], i)->type() == 'M')
				{
					return true;
				}
				break;
			}
		}

		//right
		for (i = position[1] + 1; i <= 7; i++)
		{
			if (brdptr->at(position[0], i) != nullptr)
			{
				if (brdptr->at(position[0], i)->type() == 'M')
				{
					return true;
				}
				break;
			}
		}
	}
	return false;
}

bool Queen::move(int from[2], int to[2], Board* brdptr)
{
	int i;
	int j;
	Piece* ptr;

	//check if same piece
	if (from[0] == to[0] && from[1] == to[1])
	{
		return false;
	}

	//check if moves like rook
	if (from[0] == from[1])
	{
		if (from[1] < to[1])
		{
			for (i = from[1] + 1; i < to[1]; i++)
			{
				if (brdptr->at(from[0], i) != nullptr)
				{
					return false;
				}
			}
		}
		else if (from[1] > to[1])
		{
			for (i = from[1] - 1; i > to[1]; i--)
			{
				if (brdptr->at(from[0], i) != nullptr)
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	else if (from[1] == to[1])
	{
		if (from[0] < to[0])
		{
			for (i = from[0] + 1; i < to[0]; i++)
			{
				if (brdptr->at(i, from[1]) != nullptr)
				{
					return false;
				}
			}
		}
		else if (from[0] > to[0])
		{
			for (i = from[0] - 1; i > to[0]; i--)
			{
				if (brdptr->at(i, from[1]) != nullptr)
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}

	//check if moves like bishop
	else if (from[0] - to[0] == from[1] - to[1] || from[0] - to[0] == to[1] - from[1])
	{
		//check if in the way
		if (to[0] > from[0] && to[1] > from[1]) //right & down
		{
			i = from[0] + 1;
			j = from[1] + 1;
			while (i < to[0] && j < to[1])
			{
				if (brdptr->at(i, j) != nullptr)
				{
					return false;
				}
				i++;
				j++;
			}
		}
		else if (to[0] < from[0] && to[1] > from[1])
		{
			i = from[0] - 1;
			j = from[1] + 1;
			while (i>to[0] && j < to[1])
			{
				if (brdptr->at(i, j) != nullptr)
				{
					return false;
				}
				i--;
				j++;
			}
		}
		else if (to[0] < from[0] && to[1] < from[1])
		{
			i = from[0] - 1;
			j = from[1] - 1;
			while (i>to[0] && j > to[1])
			{
				if (brdptr->at(i, j) != nullptr)
				{
					return false;
				}
				i--;
				j--;
			}
		}
		else if (to[0] > from[0] && to[1] < from[1])
		{
			i = from[0] + 1;
			j = from[1] - 1;
			while (i<to[0] && j > to[1])
			{
				if (brdptr->at(i, j) != nullptr)
				{
					return false;
				}
				i++;
				j--;
			}
		}
	}

	else
	{
		return false;
	}

	//check if friendly
	if (brdptr->at(to[0], to[1]) != nullptr)
	{
		if (brdptr->at(to[0], to[1])->player() == this->_player)
		{
			return false;
		}
	}

	//move
	ptr = brdptr->at(to[0], to[1]);
	brdptr->place(this, to[0], to[1]);
	brdptr->place(nullptr, from[0], from[1]);

	if (brdptr->check(!this->_player) == true)
	{
		brdptr->place(ptr, to[0], to[1]);
		brdptr->place(this, from[0], from[1]);
		return false;
	}
	if (ptr != nullptr)
	{
		delete[] ptr;
	}
	return true;
}

char Queen::type()
{
	if (this->_player == white)
	{
		return 'Q';
	}
	else
	{
		return 'q';
	}
}