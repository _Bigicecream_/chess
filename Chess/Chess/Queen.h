#ifndef QUEEN_H
#define QUEEN_H

#include <string>
#include "Piece.h"

class Queen : public Piece
{
public:
	Queen(bool player, int y, int x);
	bool move(int from[2],int to[2], Board* brdptr);
	bool check(int position[2],Board* brdptr);
	char type();
};

#endif // !QUEEN_H