#include "Chess.h"

Board::Board()
{
	int i;
	int j;
	this->board[0][0] = new Rook(black,0,0);
	this->board[0][1] = new Knight(black,0,1);
	this->board[0][2] = new Bishop(black,0,2);
	this->board[0][3] = new Queen(black,0,3);
	this->board[0][4] = new King(black,0,4);
	this->board[0][5] = new Bishop(black,0,5);
	this->board[0][6] = new Knight(black,0,6);
	this->board[0][7] = new Rook(black,0,7);
	for (i = 0; i < 8; i++)
	{
		this->board[1][i] = new Pawn(black,1,i);
	}
	for (i = 2; i < 6; i++)
	{
		for (j = 0; j < 8; j++)
		{
			this->board[i][j] = nullptr;
		}
	}
	for (i = 0; i < 8; i++)
	{
		this->board[6][i] = new Pawn(white,6,i);
	}
	this->board[7][0] = new Rook(white,7,0);
	this->board[7][1] = new Knight(white,7,1);
	this->board[7][2] = new Bishop(white,7,2);
	this->board[7][3] = new Queen(white,7,3);
	this->board[7][4] = new King(white,7,4);
	this->board[7][5] = new Bishop(white,7,5);
	this->board[7][6] = new Knight(white,7,6);
	this->board[7][7] = new Rook(white,7,7);
}

bool Board::move(int from[2], int to[2], bool player)
{
	if (from[0] >= 0 && from[0] < 8 && from[1] >= 0 && from[1] < 8 && to[0] >= 0 && to[0] < 8 && to[1] >= 0 && to[1] < 8)
	{
		if (this->board[from[0]][from[1]] != nullptr)
		{
			if (board[from[0]][from[1]]->player() == player)
			{
				if (board[from[0]][from[1]]->move(from, to, this))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}

		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	
}
bool Board::check(bool player)
{
	int arr[2];
	for (arr[0] = 0; arr[0] < 8; arr[0]++)
	{
		for (arr[1] = 0; arr[1] < 8; arr[1]++)
		{
			if (this->board[arr[0]][arr[1]] != nullptr && this->board[arr[0]][arr[1]]->player() == player)
			{
				if (this->board[arr[0]][arr[1]]->check(arr, this) == true)
				{
					return true;
				}
			}
		}
	}
	return false;;
}

void Board::print()
{
	HANDLE hConsole;
	int i;
	int j;
	char chY, chX;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	cout << "  ";
	for (j = 0; j < 8; j++)
	{
		SetConsoleTextAttribute(hConsole, 140);
		chY = 'A' + j;
		cout << chY << " ";
	}
	cout << "" << endl;
	for (j = 0; j < 8; j++)
	{
		SetConsoleTextAttribute(hConsole, 140);
		chX = '8' - j;
		cout << chX << " ";
		for (i = 0; i < 8; i++)
		{
			if (this->board[j][i] == nullptr)
			{
				SetConsoleTextAttribute(hConsole, 129);
				cout << "- ";
			}
			else
			{
				if (this->board[j][i]->type()<='Z')
				{
					SetConsoleTextAttribute(hConsole, 143);
				}
				
				else
				{
					SetConsoleTextAttribute(hConsole, 128);
				}
				cout <<this->board[j][i]->type() << " ";
			}
		}
		cout << "" << endl;
	}
	SetConsoleTextAttribute(hConsole, 129);
}

Piece* Board::at(int y,int x)
{
	return this->board[y][x];
}

int Board::save(string saveName,bool player)
{
	int y, x;
	string path = "saves/" + saveName+".txt";
	ofstream myfile(path);
	if (myfile.is_open())
	{
		for (y = 0; y < 8; y++)
		{
			for (x = 0; x < 8; x++)
			{
				if (this->board[y][x] == nullptr)
				{
					myfile << "0";
				}
				else
				{
					myfile << this->board[y][x]->type();
				}
			}
		}
		if (player == white)
		{
			myfile << 'w';
		}
		else
		{
			myfile << 'b';
		}
		myfile.close();
	}
	else
	{
		return 1;
	}
	return 0;
}

Board::Board(string saveName, int* flag)
{
	int x, y,counter=0;
	string Thesavefile;
	string path = "saves/" + saveName + ".txt";
	ifstream myfile(path);
	*flag = 0;
	if (myfile.is_open())
	{
		getline(myfile, Thesavefile);
		myfile.close();
		/*Put on the board arr the pieces*/
		for (y = 0; y < 8; y++)
		{
			for (x = 0; x < 8; x++)
			{
				if (Thesavefile[counter] == 'B')
				{
					board[y][x] = new Bishop(white, y, x);
				}
				else if (Thesavefile[counter] == 'b')
				{
					board[y][x] = new Bishop(black, y, x);
				}
				///////////////////////////////////////////////////
				if (Thesavefile[counter] == 'M')
				{
					board[y][x] = new King(white, y, x);
				}
				if (Thesavefile[counter] == 'm')
				{
					board[y][x] = new King(black, y, x);
				}
				///////////////////////////////////////////////////
				if (Thesavefile[counter] == 'K')
				{
					board[y][x] = new Knight(white, y, x);
				}
				if (Thesavefile[counter] == 'k')
				{
					board[y][x] = new Knight(black, y, x);
				}
				///////////////////////////////////////////////////
				if (Thesavefile[counter] == 'P')
				{
					board[y][x] = new Pawn(white, y, x);
				}
				if (Thesavefile[counter] == 'p')
				{
					board[y][x] = new Pawn(black, y, x);
				}
				///////////////////////////////////////////////////
				if (Thesavefile[counter] == 'Q')
				{
					board[y][x] = new Queen(white, y, x);
				}
				if (Thesavefile[counter] == 'q')
				{
					board[y][x] = new Queen(black, y, x);
				}
				///////////////////////////////////////////////////
				if (Thesavefile[counter] == 'R')
				{
					board[y][x] = new Rook(white, y, x);
				}
				if (Thesavefile[counter] == 'r')
				{
					board[y][x] = new Rook(black, y, x);
				}
				///////////////////////////////////////////////////
				if (Thesavefile[counter] == '0')
				{
					board[y][x] = nullptr;
				}
				counter++;
			}

		}
		if (Thesavefile[64] == 'w')
		{
			*flag = 1;
		}
		else
		{
			*flag = 2;
		}
	}
	else
	{
		*flag = -1;
	}
	
}


Board::~Board()
{
	int i;
	int j;
	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			if (this->board[i][j] != nullptr)
			{
				delete this->board[i][j];
			}
			this->board[i][j] = nullptr;
		}
	}
}

void Board::place(Piece* ptr,int y, int x)
{
	this->board[y][x] = ptr;
}

void Board::game(bool player)
{
	bool turn = player;
	string fromstr;
	string tostr;
	string name;
	bool flag;
	int from[2];
	int to[2];
	system("CLS");
	cout << "Type quit in any place to quit your game" << endl;
	cout << "Type save in any place to save your game" << endl;
	cout << "Enjoy!" << endl;
	while (true)
	{
		cout << endl << endl;
		this->print();
		cout << endl;
		if (turn == white)
		{
			cout << "White player's turn" << endl;
		}
		else
		{
			cout << "Black player's turn" << endl;
		}
		if (this->check(!turn) == true)
		{
			if (turn == white)
			{
				cout << "BLACK PLAYER CHECK" << endl;
			}
			else
			{
				cout << "WHIET PLAYER CHECK" << endl;
			}
			
		}
		while(true) // from loop
		{
			flag = false;
			cout << "From: ";
			cin >> fromstr;
			if (fromstr.compare("quit") == 0 || fromstr.compare("QUIT") == 0 || fromstr.compare("Quit") == 0)
			{
				return;
			}
			else if (fromstr.compare("save") == 0 || fromstr.compare("SAVE") == 0 || fromstr.compare("Save") == 0)
			{
				cout << "What would you like to name your save? ";
				cin >> name;
				if (this->save(name,turn) == 0)
				{
					cout << "Your game has been saved" << endl;
				}
				else
				{
					cout << "Unable to save your game" << endl;
				}
			}
			else if (fromstr.length() == 2)
			{
				cout << "To: ";
				cin >> tostr;
				if (fromstr.compare("quit") == 0 || fromstr.compare("QUIT") == 0 || fromstr.compare("Quit") == 0)
				{
					return;
				}
				else if (fromstr.compare("save") == 0 || fromstr.compare("SAVE") == 0 || fromstr.compare("Save") == 0)
				{
					cout << "What would you like to name your save? ";
					cin >> name;
					if (this->save(name,turn) == 0)
					{
						cout << "Your game has been saved" << endl;
					}
					else
					{
						cout << "Unable to save your game" << endl;
					}
				}
				else if (tostr.length() == 2)
				{
					if (fromstr[0] >= 'A' && fromstr[0] <= 'H' && fromstr[1] >= '1' && fromstr[1] <= '8')
					{
						if (tostr[0] >= 'A' && tostr[0] <= 'H' && tostr[1] >= '1' && tostr[1] <= '8')
						{
							parse(fromstr, from);
							parse(tostr, to);
							if (this->move(from, to, turn) == true)
							{
								flag = true;
							}
						}
					}
				}
			}
			if (flag == true)
			{
				turn = !turn;
				break;
			}
		}
	}
}

bool Board::checkmate(bool player)
{
	int i;
	int j;
	int from[2];
	int to[2];
	Piece* king = nullptr;
	Piece* temp = nullptr;
	bool flag = false;
	return false;
	for (i = 0; i < 8 && flag == false; i++)
	{
		for (j = 0; j < 8 && flag == false; j++)
		{
			if (player == white)
			{
				if (this->board[i][j]->type() == 'M')
				{
					flag = true;
					king = board[i][j];
				}
			}
			else
			{
				if (this->board[i][j]->type() == 'm')
				{
					flag = true;
					king = this->board[i][j];
				}
			}
			
		}
	}
	
	flag = false;
	if (king->y() > 0)
	{
		if (this->board[king->y() - 1][king->x()] != nullptr)
		{
			temp = this->board[king->y() - 1][king->x()];
			from[1] = king->x();
			from[0] = king->y();
			to[0] = temp->y();
			to[1] = temp->x();
			if (this->move(from,to,player) == true)
			{

			}
		}
	}

}
