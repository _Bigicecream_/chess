#include "Chess.h"

void parse(string str, int arr[2])
{
	if (str[0] <= 'Z'&&str[0] >= 'A')
	{
		arr[1] = str[0] - 'A';
	}
	else
	{
		arr[1] = str[0] - 'a';
	}
	arr[0] = '8' - str[1];
}
int readMovesFromFile(string filename, Board* brdptr)
{
	string line;
	bool turn=false;
	int from[2];
	int to[2];
	string fromstr;
	string tostr;
	string path = "moves/" + filename + ".txt";
	ifstream myfile(path);
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			turn = !turn;
			fromstr += line[0];
			fromstr += line[1];
			tostr += line[3];
			tostr += line[4];
			parse(fromstr, from);
			parse(tostr, to);
			brdptr->move(from, to, turn);
			brdptr->print();
			fromstr.clear();
			tostr.clear();
		}
		myfile.close();
	}
	else
	{
		cout << "Unable to open file";
		return 1;
	}

	return 0;
}

