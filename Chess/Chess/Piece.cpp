#include "Chess.h"

bool Piece::player()
{
	return this->_player;
}

int Piece::x()
{
	return this->_x;
}

int Piece::y()
{
	return this->_y;
}