#ifndef KNIGHT_H
#define KNIGHT_H

#include <string>
#include "Piece.h"

class Knight : public Piece
{
public:
	Knight(bool player, int y, int x);
	bool move(int from[2], int to[2], Board* brdptr);
	bool check(int position[2],Board* brdptr);
	char type();
};

#endif // !KNIGHT_H