#include "Board.h"
#include "Piece.h"
#include "Pawn.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"


#include <string>
#include <iostream>
#include <fstream>
#include<windows.h>

#define white true
#define black false

void parse(string str, int arr[2]);
int readMovesFromFile(string filename, Board* brdptr);