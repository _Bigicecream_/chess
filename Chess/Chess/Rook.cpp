#include "Chess.h"

Rook::Rook(bool player, int y, int x)
{
	this->_player = player;
	this->_y = y;
	this->_x = x;
}

bool Rook::check(int position[2], Board* brdptr)
{
	int i;
	if (this->player() == white)
	{
		//up
		
		for (i = position[0] - 1; i >= 0; i--)
		{
			if (brdptr->at(i, position[1]) != nullptr)
			{
				if (brdptr->at(i, position[1])->type() == 'm')
				{
					return true;
				}
				break;
			}
		}
		
		//down
		for (i = position[0] + 1; i <= 7; i++)
		{
			if (brdptr->at(i, position[1]) != nullptr)
			{
				if (brdptr->at(i, position[1])->type() == 'm')
				{
					return true;
				}
				break;
			}
		}

		//left
		for (i = position[1] - 1; i >= 0; i--)
		{
			if (brdptr->at(position[0], i) != nullptr)
			{
				if (brdptr->at(position[0], i)->type() == 'm')
				{
					return true;
				}
				break;
			}
		}

		//right
		for (i = position[1] + 1; i <= 7; i++)
		{
			if (brdptr->at(position[0], i) != nullptr)
			{
				if (brdptr->at(position[0], i)->type() == 'm')
				{
					return true;
				}
				break;
			}
		}
	}
	else
	{
		//up
		for (i = position[0] - 1; i >= 0; i--)
		{
			if (brdptr->at(i, position[1]) != nullptr)
			{
				if (brdptr->at(i, position[1])->type() == 'M')
				{
					return true;
				}
				break;
			}
		}

		//down
		for (i = position[0] + 1; i <= 7; i++)
		{
			if (brdptr->at(i, position[1]) != nullptr)
			{
				if (brdptr->at(i, position[1])->type() == 'M')
				{
					return true;
				}
				break;
			}
		}

		//left
		for (i = position[1] - 1; i >= 0; i--)
		{
			if (brdptr->at(position[0], i) != nullptr)
			{
				if (brdptr->at(position[0], i)->type() == 'M')
				{
					return true;
				}
				break;
			}
		}

		//right
		for (i = position[1] + 1; i <= 7; i++)
		{
			if (brdptr->at(position[0], i) != nullptr)
			{
				if (brdptr->at(position[0], i)->type() == 'M')
				{
					return true;
				}
				break;
			}
		}
	}
	return false;
}

bool Rook::move(int from[2],int to[2], Board* brdptr)
{
	int i;
	Piece* ptr;
	//check if there's a piece in the way
	if (from[0] == to[0])
	{
		if (from[1] < to[1])
		{
			for (i = from[1] + 1; i < to[1]; i++)
			{
				if (brdptr->at(from[0], i) != nullptr)
				{
					return false;
				}
			}
		}
		else if (from[1] > to[1])
		{
			for (i = from[1] - 1; i > to[1]; i--)
			{
				if (brdptr->at(from[0], i) != nullptr)
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	else if (from[1] == to[1])
	{
		if (from[0] < to[0])
		{
			for (i = from[0] + 1; i < to[0]; i++)
			{
				if (brdptr->at(i, from[1]) != nullptr)
				{
					return false;
				}
			}
		}
		else if (from[0] > to[0])
		{
			for (i = from[0] - 1; i > to[0]; i--)
			{
				if (brdptr->at(i, from[1]) != nullptr)
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	//check if there is not friendly on that square
	if (brdptr->at(to[0], to[1]) != nullptr)
	{
		if (brdptr->at(to[0], to[1])->player() == this->_player)
		{
			return false;
		}
	}

	//start moving
	ptr = brdptr->at(to[0], to[1]);
	brdptr->place(brdptr->at(from[0], from[1]), to[0], to[1]);
	brdptr->place(nullptr, from[0], from[1]);
	if (brdptr->check(!this->_player) == true)
	{
		brdptr->place(this, from[0], from[1]);
		brdptr->place(ptr, to[0], to[1]);
		return false;
	}

	if (ptr != nullptr)
	{
		delete[] ptr;
	}
	return true;
}

char Rook::type()
{
	if (this->_player == white)
	{
		return 'R';
	}
	else
	{
		return 'r';
	}
}