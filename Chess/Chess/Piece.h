#ifndef PIECE
#define PIECE

#include <string>

using namespace std;
class Board;
class Piece
{
protected:
	bool _player;
	int _x;
	int _y;
public:
	virtual char type() = 0;
	bool player();
	virtual bool check(int position[2], Board* brdptr) = 0;
	virtual bool move(int from[2],int to[2], Board* brdptr) = 0;
	int x();
	int y();
};

#endif