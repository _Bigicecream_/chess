#include "Chess.h"

Bishop::Bishop(bool player,int y, int x)
{
	this->_player = player;
	this->_y = y;
	this->_x = x;
}

bool Bishop::move(int from[2], int to[2], Board* brdptr)
{
	int i;
	int j;
	Piece* ptr;
	//check if same piece
	if (from[0] == to[0] && from[1] == to[1])
	{
		return false;
	}

	//Check if legal
	if (from[0] - to[0] != from[1] - to[1] && from[0] - to[0] != to[1] - from[1])
	{
		return false;
	}

	//check if in the way
	if (to[0] > from[0] && to[1] > from[1]) //right & down
	{
		i = from[0] + 1;
		j = from[1] + 1;
		while (i < to[0] && j < to[1])
		{
			if (brdptr->at(i, j) != nullptr)
			{
				return false;
			}
			i++;
			j++;
		}
	}
	else if (to[0] < from[0] && to[1] > from[1])
	{
		i = from[0] - 1;
		j = from[1] + 1;
		while (i>to[0] && j < to[1])
		{
			if (brdptr->at(i, j) != nullptr)
			{
				return false;
			}
			i--;
			j++;
		}
	}
	else if (to[0] < from[0] && to[1] < from[1])
	{
		i = from[0] - 1;
		j = from[1] - 1;
		while (i>to[0] && j > to[1])
		{
			if (brdptr->at(i, j) != nullptr)
			{
				return false;
			}
			i--;
			j--;
		}
	}
	else if (to[0] > from[0] && to[1] < from[1])
	{
		i = from[0] + 1;
		j = from[1] - 1;
		while (i<to[0] && j > to[1])
		{
			if (brdptr->at(i, j) != nullptr)
			{
				return false;
			}
			i++;
			j--;
		}
	}
	else
	{
		return false;
	}

	//check if friendly in the way
	if (brdptr->at(to[0], to[1]) != nullptr)
	{
		if (brdptr->at(to[0], to[1])->player() == this->_player)
		{
			return false;
		}


	}

	//move
	ptr = brdptr->at(to[0], to[1]);
	brdptr->place(this, to[0], to[1]);
	brdptr->place(nullptr, from[0], from[1]);
	if (brdptr->check(!this->_player) == true)
	{
		brdptr->place(ptr, to[0], to[1]);
		brdptr->place(this, from[0], from[1]);
		return false;
	}
	if (ptr != nullptr)
	{
		delete[] ptr;
	}
	return true;
}

bool Bishop::check(int position[2],Board* brdptr)
{
	int i, j;
	if (this->_player == white)
	{
		//rightup
		i = position[0] - 1;
		j = position[1] + 1;
		while (i >= 0 && j <= 7)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'm')
				{
					return true;
				}
				break;
			}
			i--;
			j++;
		}

		//leftdown
		i = position[0] + 1;
		j = position[1] - 1;
		while (i <= 7 && j >= 0)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'm')
				{
					return true;
				}
				break;
			}
			i++;
			j--;
		}

		//rightdown
		i = position[0] + 1;
		j = position[1] + 1;
		while (i <= 7 && j <= 7)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'm')
				{
					return true;
				}
				break;
			}
			i++;
			j++;
		}

		//leftup
		i = position[0] - 1;
		j = position[1] - 1;
		while (i >= 0 && j >= 0)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'm')
				{
					return true;
				}
				break;
			}
			i--;
			j--;
		}
	}
	else
	{
		//rightup
		i = position[0] - 1;
		j = position[1] + 1;
		while (i >= 0 && j <= 7)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'M')
				{
					return true;
				}
				break;
			}
			i--;
			j++;
		}
		//leftdown
		i = position[0] + 1;
		j = position[1] - 1;
		while (i <= 7 && j >= 0)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'M')
				{
					return true;
				}
				break;
			}
			i++;
			j--;
		}
		//rightdown
		i = position[0] + 1;
		j = position[1] + 1;
		while (i <= 7 && j <= 7)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'M')
				{
					return true;
				}
				break;
			}
			i++;
			j++;
		}
		//leftup
		i = position[0] - 1;
		j = position[1] - 1;
		while (i >= 0 && j >= 0)
		{
			if (brdptr->at(i, j) != nullptr)
			{
				if (brdptr->at(i, j)->type() == 'M')
				{
					return true;
				}
				break;
			}
			i--;
			j--;
		}
	}
	return false;
}

char Bishop::type()
{
	if (this->_player == white)
	{
		return 'B';
	}
	else
	{
		return 'b';
	}
}

