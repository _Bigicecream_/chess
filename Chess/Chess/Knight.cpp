#include "Chess.h"

Knight::Knight(bool player,int y, int x)
{
	this->_player = player;
	this->_y = y;
	this->_x = x;
}

bool Knight::move(int from[2], int to[2], Board* brdptr)
{
	//check if move is legal
	Piece* ptr;
	bool flag = false;
	if (from[0] > 0 && from[1] < 6)
	{
		if (from[0] - to[0] == 1 && to[1] - from[1] == 2)
		{
			flag = true;
		}
	}
	if (from[0] > 1 && from[1] < 7)
	{
		if (from[0] - to[0] == 2 && to[1] - from[1] == 1)
		{
			flag = true;
		}
	}
	if (from[0] > 1 && from[1] > 0)
	{
		if (from[0] - to[0] == 2 && from[1] - to[1] == 1)
		{
			flag = true;
		}
	}
	if (from[0] > 0 && from[1] > 1)
	{
		if (from[0] - to[0] == 1 && from[1] - to[1] == 2)
		{
			flag = true;
		}
	}
	if (from[0] < 7 && from[1] > 1)
	{
		if (to[0] - from[0] == 1 && from[1] - to[1] == 2)
		{
			flag = true;
		}
	}
	if (from[0] < 6 && from[1] > 0)
	{
		if (to[0] - from[0] == 2 && from[1] - to[1] == 1)
		{
			flag = true;
		}
	}
	if (from[0] < 6 && from[1] < 7)
	{
		if (to[0] - from[0] == 2 && to[1] - from[1] == 1)
		{
			flag = true;
		}
	}
	if (from[0] < 7 && from[1] < 6)
	{
		if (to[0] - from[0] == 1 && to[1] - from[1] == 2)
		{
			flag = true;
		}
	}

	//check above
	if (flag == false)
	{
		return false;
	}

	//check if there is no friendly in the way
	if (brdptr->at(to[0], to[1]) != nullptr && brdptr->at(to[0], to[1])->player() == this->_player)
	{
		return false;
	}

	//move
	ptr = brdptr->at(to[0], to[1]);
	brdptr->place(this, to[0], to[1]);
	brdptr->place(nullptr, from[0], from[1]);

	//check if check
	if (brdptr->check(!this->_player) == true)
	{
		brdptr->place(this, from[0], from[1]);
		brdptr->place(ptr, to[0], to[1]);
		return false;
	}
	if (ptr != nullptr)
	{
		delete[] ptr;
	}
	return true;
}

bool Knight::check(int position[2], Board* brdptr)
{
	if (this->_player == white)
	{
		if (position[0] < 6 && position[1] < 7)
		{
			if (brdptr->at(position[0] + 2, position[1] + 1) != nullptr)
			{
				if (brdptr->at(position[0] + 2, position[1] + 1)->type() == 'm')
				{
					return true;
				}
			}
		}
		if (position[0] > 1 && position[1] < 7)
		{
			if (brdptr->at(position[0] - 2, position[1] + 1) != nullptr)
			{
				if (brdptr->at(position[0] - 2, position[1] + 1)->type() == 'm')
				{
					return true;
				}
			}
		}
		if (position[0] < 6 && position[1] > 0)
		{
			if (brdptr->at(position[0] + 2, position[1] - 1) != nullptr)
			{
				if (brdptr->at(position[0] + 2, position[1] - 1)->type() == 'm')
				{
					return true;
				}
			}
		}
		if (position[0] > 1 && position[1] > 0)
		{
			if (brdptr->at(position[0] - 2, position[1] - 1) != nullptr)
			{
				if (brdptr->at(position[0] - 2, position[1] - 1)->type() == 'm')
				{
					return true;
				}
			}
		}
		if (position[0] < 7 && position[1] < 6)
		{
			if (brdptr->at(position[0] + 1, position[1] + 2) != nullptr)
			{
				if (brdptr->at(position[0] + 1, position[1] + 2)->type() == 'm')
				{
					return true;
				}
			}
		}
		if (position[0] < 7 && position[1] > 1)
		{
			if (brdptr->at(position[0] + 1, position[1] - 2) != nullptr)
			{
				if (brdptr->at(position[0] + 1, position[1] - 2)->type() == 'm')
				{
					return true;
				}
			}
		}
		if (position[0] > 0 && position[1] < 6)
		{
			if (brdptr->at(position[0] - 1, position[1] + 2) != nullptr)
			{
				if (brdptr->at(position[0] - 1, position[1] + 2)->type() == 'm')
				{
					return true;
				}
			}
		}
		if (position[0]>0 && position[1] > 1)
		{
			if (brdptr->at(position[0] - 1, position[1] - 2) != nullptr)
			{
				if (brdptr->at(position[0] - 1, position[1] - 2)->type() == 'm')
				{
					return true;
				}
			}
		}
	}

	else
	{
		if (position[0] < 6 && position[1] < 7)
		{
			if (brdptr->at(position[0] + 2, position[1] + 1) != nullptr)
			{
				if (brdptr->at(position[0] + 2, position[1] + 1)->type() == 'M')
				{
					return true;
				}
			}
		}
		if (position[0] > 1 && position[1] < 7)
		{
			if (brdptr->at(position[0] - 2, position[1] + 1) != nullptr)
			{
				if (brdptr->at(position[0] - 2, position[1] + 1)->type() == 'M')
				{
					return true;
				}
			}
		}
		if (position[0] < 6 && position[1] > 0)
		{
			if (brdptr->at(position[0] + 2, position[1] - 1) != nullptr)
			{
				if (brdptr->at(position[0] + 2, position[1] - 1)->type() == 'M')
				{
					return true;
				}
			}
		}
		if (position[0] > 1 && position[1] > 0)
		{
			if (brdptr->at(position[0] - 2, position[1] - 1) != nullptr)
			{
				if (brdptr->at(position[0] - 2, position[1] - 1)->type() == 'M')
				{
					return true;
				}
			}
		}
		if (position[0] < 7 && position[1] < 6)
		{
			if (brdptr->at(position[0] + 1, position[1] + 2) != nullptr)
			{
				if (brdptr->at(position[0] + 1, position[1] + 2)->type() == 'M')
				{
					return true;
				}
			}
		}
		if (position[0] < 7 && position[1] > 1)
		{
			if (brdptr->at(position[0] + 1, position[1] - 2) != nullptr)
			{
				if (brdptr->at(position[0] + 1, position[1] - 2)->type() == 'M')
				{
					return true;
				}
			}
		}
		if (position[0] > 0 && position[1] < 6)
		{
			if (brdptr->at(position[0] - 1, position[1] + 2) != nullptr)
			{
				if (brdptr->at(position[0] - 1, position[1] + 2)->type() == 'M')
				{
					return true;
				}
			}
		}
		if (position[0]>0 && position[1] > 1)
		{
			if (brdptr->at(position[0] - 1, position[1] - 2) != nullptr)
			{
				if (brdptr->at(position[0] - 1, position[1] - 2)->type() == 'M')
				{
					return true;
				}
			}
		}
	}
	return false;
}

char Knight::type()
{
	if (this->_player == white)
	{
		return 'K';
	}
	else
	{
		return 'k';
	}
}